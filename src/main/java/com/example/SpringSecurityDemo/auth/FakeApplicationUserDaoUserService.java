package com.example.SpringSecurityDemo.auth;

import com.example.SpringSecurityDemo.security.ApplicationUserPermission;
import com.example.SpringSecurityDemo.security.ApplicationUserRole;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("fake")
public class FakeApplicationUserDaoUserService implements ApplicationUserDao {
    private final PasswordEncoder  passwordEncoder;
    @Autowired
    public FakeApplicationUserDaoUserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<ApplicationUser> selectApplicationByUsername(String username) {
        return getApplicationUsers()
                .stream()
                .filter(applicationUser -> username.equals(applicationUser.getUsername()))
                .findFirst();
    }

    private List<ApplicationUser> getApplicationUsers(){
        List<ApplicationUser> applicationUser = Lists.newArrayList(
                new ApplicationUser(ApplicationUserRole.STUDENT.getGrantedAuthorities(),
                        passwordEncoder.encode("password"),
                        "annasmith",
                        true,
                        true,
                        true),
                new ApplicationUser(ApplicationUserRole.ADMIN.getGrantedAuthorities(),
                        passwordEncoder.encode("password123"),
                        "linda",
                        true,
                        true,
                        true),
                new ApplicationUser(ApplicationUserRole.ADMINTRAINEE.getGrantedAuthorities(),
                        passwordEncoder.encode("password123"),
                        "tom",
                        true,
                        true,
                        true)
        );
        return applicationUser;
    }
}
