package com.example.SpringSecurityDemo.auth;

import java.util.Optional;

public interface ApplicationUserDao {
    public Optional<ApplicationUser> selectApplicationByUsername(String username);
}
